/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-chart')

/**
 * @ngdoc service
 * @name $$amWbChartDataUtil
 * @description Data utility
 * 
 * Some basic data mainpulation tools.
 * 
 */
.service('$amWbChartDataUtil', function() {

	/**
	 * Format data as local date
	 * 
	 * Options:
	 * <ul>
	 * 	<li>type: data format (DateTime, )</li>
	 * 	<li>format: string to format data</li>
	 * </ul>
	 * 
	 * <h3>DateTime</h3>
	 * 
	 * In this case data must be a UnixTime in UTC, and the option format must 
	 * be as described here: 
	 * 
	 * http://pubs.opengroup.org/onlinepubs/009695399/functions/strptime.html
	 * 
	 * <h3>Default format</h3>
	 * 
	 * By default we suppose data is an number value and try to format as described
	 * here:
	 * 
	 * https://docs.python.org/3/library/string.html#format-specification-mini-language
	 * 
	 * 
	 * @param data value
	 * @param option to format data
	 */
	function formatData(data, option){
		if (option.type === 'DateTime'){
			var date = moment.unix(data);
			return d3.time.format(option.format || '%x')(date.toDate());
		}
		return d3.format(option.format || '.xx')(data);
	}
	
	/**
	 * Search and replace data in sheet
	 * 
	 * 
	 * query: a search query
	 * replace: a replacement
	 * range: where to search (may be empty)
	 * 
	 * match: true/false matche the case
	 * regex: true/false match based on regex
	 * formula: true/false search in formula
	 */
	function findAndReplace(sheet, option){
		// Replace with regex
		if(option.regex && option.match){
			var regex = new RegExp(option.query, 'ig');
			angular.forEach(sheet.values, function(row, i){
				angular.forEach(row, function(cell, j){
					if(regex.test(cell)){
						sheet.values[i][j] = option.replace;
					}
				});
			});
		}
		// TODO: support the others
	}
	
	
	function appendSheetsAsRow(dataSheet){
		var sheet = {
				key: 'New sheet',
				values:[]
		};
		for(var i = 0; i < dataSheet.length; i++){
			for(var j = 0; j < dataSheet[i].values.length; j++){
				sheet.values.push(dataSheet[i].values[j]);
			}
		}
		return sheet;
	}
	
	function appendTextToColumn(text, column, sheet){
		for(var i = 0; i < sheet.values.length; i++){
			sheet.values[i][column] = text + sheet.values[i][column];
		}
	}
	
	// public methods
	this.formatData = formatData;
	this.findAndReplace = findAndReplace; 
	this.appendSheetsAsRow = appendSheetsAsRow;
	this.appendTextToColumn = appendTextToColumn;
});

