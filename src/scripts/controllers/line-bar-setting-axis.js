/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * setting for X and Y Axis of the graph widget
 *
 * author mgh
 */
angular.module('am-wb-chart')
/**
 *
 */
.controller('AmWbChartLineBarSettingAxisCtrl', function($scope) {
	$scope.formats =[{
		title : 'Date time',
		icon : 'wb-vertical-boxes',
		value : 'DateTime'
	},{
		title : 'Number',
		icon : 'wb-vertical-boxes',
		value : 'Number'
	}];

	var patterns = {
			DateTime : [{
				title : '2017-09-25',
				icon : 'wb-vertical-boxes',
				value : '%Y-%m-%d'
			}, {
				title : '2017-09-25T08:15:30',
				icon : 'wb-vertical-boxes',
				value : '%Y-%m-%dT%H:%M:%S'
			}, {
				title : '08:15:30',
				icon : 'wb-vertical-boxes',
				value : '%H:%M:%S'
			}],
			Number : [{
				title : 'x',
				icon : 'wb-vertical-boxes',
				value : '.0f'
			}, {
				title : '.x',
				icon : 'wb-vertical-boxes',
				value : '.1f'
			}, {
				title : '.xx',
				icon : 'wb-horizontal-boxes',
				value : '.2f'
			}, {
				title : '.xxx',
				icon : 'wb-horizontal-boxes',
				value : '.3f'
			}, {
				title : 'K/M/G/T',
				icon : 'wb-horizontal-boxes',
				value : '.3s'
			}, {
				title : 'Thousands',
				icon : 'wb-horizontal-boxes',
				value : ',.2r'
			}]
	};

	/**
	 * Get patterns of a format
	 */
	$scope.getPatterns = function (type){
		return patterns[type];
	};

});