/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-line-bar module
 *
 * author: mgh
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('am-wb-chart')
/**
 *
 */
.controller('AmWbChartBoxWidgetCtrl', function($scope, $amWbChartPrometheus,$amWbChartDataUtil, $q) {

    /*
     * Store the chart option
     */
    this.chart = {
            type : 'boxPlotChart',
            height : 450,
            margin : {
                top : 20,
                right : 20,
                bottom : 60,
                left : 40
            },
            color : [ 'darkblue', 'darkorange', 'green', 'darkred', 'darkviolet' ],
            maxBoxWidth : 75,
            x: function(d) {
                return d[0];
            },
            q1: function(d) {
                return d[1];
            },
            q2: function(d) {
                return d[2];
            }, 
            q3: function(d) {
                return d[3];
            },
            wl: function(d) {
                return d[4];
            },
            wh: function(d) {
                return d[5];
            },
            outliers: function (d) { 
                if(d.length <= 6){
                    return [];
                }
                return d.slice(6, d.length);
            }
    };


    /**
     * Load widget init data
     */
    function loadPreData() {
        var result = null;
        if(angular.isDefined(ngModel.query)){
            result = $amWbChartPrometheus.run(ngModel.query)//
            .then(function(data){
                for(var i = 0; i < data.length; i++){
                    $amWbChartDataUtil.appendTextToColumn(data[i].key, 0, data[i]);
                }
                ngModel.data = $amWbChartDataUtil.appendSheetsAsRow(data);
            });
        } else {
            ngModel.data = {
                    key : 'Box data',
                    values : [
                        //[label, Q1, Q2, Q3, low, high, ... outliers]
                        [ 'label1', 100, 200, 300, 50, 350, 10, 360, 355],
                        [ 'label2', 100, 200, 300, 50, 350, 10, 360, 355 ],
                        [ 'label3', 100, 200, 300, 50, 350, 10, 360, 355 ]
                        ]
            };
        }
        if(ngModel.searchReplace){
            result = $q.when(result)//
            .then(function(){
                if(angular.isArray(ngModel.searchReplace)){
                    angular.forEach(ngModel.searchReplace, function(option){
                        $amWbChartDataUtil.findAndReplace(ngModel.data, option);
                    });
                } else {
                    $amWbChartDataUtil.findAndReplace(ngModel.data, ngModel.searchReplace);
                }
            });
        }
    }

});