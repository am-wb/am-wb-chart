/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Load widgets settigns
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('am-wb-chart')

/**
 * Load widgets
 *
 * author: mgh
 */
.run(function($settings) {

	$settings.newPage({
		type: 'table',
		label : 'Table',
		description : 'Manages a table properties',
		templateUrl : 'views/am-wb-chart-settings/table.html'
	});
	
	$settings.newPage({
		type: 'chart-legend',
		label : 'Legend',
		description : 'Configuration of chart legend',
		templateUrl : 'views/am-wb-chart-settings/legend.html'
	});
	$settings.newPage({
		type: 'chart-multi-chart',
		label : 'Multi chart',
		controller: 'AmWbChartDataMultiSettingCtrl',
		description : 'Multi chart configuration',
		templateUrl : 'views/am-wb-chart-settings/multi-chart.html'
	});
	
	//=== SECTION: CHART-GENERAL SETTINGS ===
	//chart-general title setting section
	$settings.newPage({
		type: 'chart-title',
		label : 'Chart - Title',
		description : 'Manage title setting for any chart widget',
		templateUrl : 'views/am-wb-chart-settings/title.html'
	});
	//chart-general caption setting section
	$settings.newPage({
		type: 'chart-caption',
		label : 'Chart - Caption',
		description : 'Manage caption setting for any Gazmeh chart widget',
		templateUrl : 'views/am-wb-chart-settings/caption.html'
	});
	//chart-general margin setting section
	$settings.newPage({
		type: 'chart-margin',
		label : 'Chart - Margin',
		description : 'Manage margin setting for any graph widget',
		templateUrl : 'views/am-wb-chart-settings/margin.html'
	});


	//=== SECTION: CHART-LINE-BAR SETTINGS ===
	//chart-line-bar main setting section
	$settings.newPage({
		type: 'chart-line-multi',
		label : 'Chart - Main',
		description : 'Manage main setting for chart-line-bar widget',
		controller:'AmWbChartDataMultiSettingCtrl',
		templateUrl : 'views/am-wb-chart-settings/line-multi.html'
	});
	
	//Multi data setting section
	$settings.newPage({
		type: 'chart-data-multi',
		label : 'Data sets',
		description : 'Manage data setting',
		controller:'AmWbChartDataMultiSettingCtrl',
		templateUrl : 'views/am-wb-chart-settings/data-multi.html'
	});

	//chart-line-bar axis setting section
	$settings.newPage({
		type: 'chart-axis',
		label : 'Chart - Axis',
		description : 'Manage axis setting for chart-line-bar widget',
		controller:'AmWbChartLineBarSettingAxisCtrl',
		templateUrl : 'views/am-wb-chart-settings/axis.html'
	});

	//=== SECTION: CHART-Pie SETTINGS ===
	//chart-pie main setting section
	$settings.newPage({
		type: 'chart-pie',
		label : 'Pie chart',
		description : 'Manage main setting for chart-pie widget',
		controller:'AmWbChartPieSettingCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-chart-settings/pie.html'
	});

	//=== SECTION: CHART-Box SETTINGS ===
	//chart-pie main setting section
	$settings.newPage({
		type: 'chart-box',
		label : 'Box chart',
		description : 'Manage main setting for chart-pie widget',
		controller:'AmWbChartBoxSettingCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-chart-settings/box.html'
	});
});
