/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Loads widgets
 * 
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('am-wb-chart')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 *
 * author: mgh
 */
.run(function($widget) {

	$widget.newWidget({
		type : 'MultiChart',
		title : 'Multi chart',
		description : 'Multi chart (with two Y-axise) is used to show un-homugenuse data types.',
		groups: ['chart', 'data-analysis'],
		icon : 'wb-widget-chart',
		// help
		help : 'https://gitlab.com/gazmeh',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-chart-widgets/multi-chart.html',
		controller : 'AmWbMultiChartWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-multi-chart'
			]
	});
	//Register 'chart-line-bar' widget
	$widget.newWidget({
		type : 'ChartLineBar',
		title : 'Line chart',
		description : 'Line chart widget to display data in line/bar view.',
		groups: ['chart', 'data-analysis'],
		icon : 'wb-widget-chart',
		// help
		help : 'https://gitlab.com/gazmeh',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-chart-widgets/line-bar.html',
		controller : 'AmWbChartLineBarWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-line-multi',
			'chart-axis',
			]
	});


	//Register 'bar' widget
	$widget.newWidget({
		type : 'ChartBox',
		title : 'Box Chart',
		description : 'Box chart to show erros',
		groups: ['chart', 'data-analysis'],
		icon : 'wb-widget-chart-box',
		// help
		help : 'https://gitlab.com/gazmeh',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-chart-widgets/box.html',
		controller : 'AmWbChartBoxWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-box',
			],
	});

	//Register 'pie' widget
	$widget.newWidget({
		type : 'ChartPie',
		title : 'Pie Chart',
		description : 'Pie and variation of the pie chart like the donut chart',
		groups: ['chart', 'data-analysis'],
		icon : 'wb-widget-chart-pie',
		// help
		help : 'https://gitlab.com/gazmeh',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-chart-widgets/pie.html',
		controller : 'AmWbChartPieWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-pie',
			]
	});

	//Register 'pie' widget
	$widget.newWidget({
		type : 'Table',
		title : 'Table',
		description : 'Display a sheet of data as a table.',
		groups: ['table', 'data-analysis'],
		icon : 'border_all',
		// help
		help : 'https://gitlab.com/gazmeh',
		helpId: '',
		// page
		templateUrl : 'views/am-wb-chart-widgets/table.html',
		controller : 'AmWbTableWidgetCtrl',
		setting : [
			'table'
			]
	});

});