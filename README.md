# Angular Material Weburger Chart

master: 
[![pipeline status - develop](https://gitlab.com/am-wb/am-wb-chart/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-chart/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4e9028e086b14ee09bbbb21d0210710b)](https://www.codacy.com/app/am-wb/am-wb-chart?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-chart&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/am-wb/am-wb-chart/badges/develop/pipeline.svg)](https://gitlab.com/am-wb/am-wb-chart/commits/develop)

Contains some weburger widgets to show chart


See more details in [technical document](https://am-wb.gitlab.io/am-wb-chart/doc/index.html).
